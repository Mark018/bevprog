#include "../std_lib_facilities.h"

int main() {
    try {
		string res = 7; //This is not INT!
		vector<int> v(10);
		v[5] = res;
		cout << "Success!\n";
        return 0;
    } catch (exception &e) {
        cerr << "Error: " << e.what() << "\n";
        return 1;
    } catch (...) {
        cerr << "Oops: unknown exception\n";
        return 2;
    }
}