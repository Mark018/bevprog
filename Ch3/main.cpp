#include "../std_lib_facilities.h"

int main()
{
	// TODO: Változók felül
	string name;
	string to_name;
	char to_sex;
	int to_age;

	string builded;

	// TODO: Bekérjük a saját nevet.
	cout << "Add meg a neved:\n";
	cin >> name;

	// TODO: Bekérjük a címzett nevét.
	cout << "Add meg a nevet annak akinek írod a levelet:\n";
	cin >> to_name;

	// TODO: Bekérjük a címzett nemét.
	cout << "Add meg a címzett nemét [m/f]:\n";
	cin >> to_sex;

	// TODO: Bekérjük a címzett korát.
	cout << "Add meg a címzett korát:\n";
	cin >> to_age;

	if (to_sex == 'm')
		builded.append("Kedves, Mr " + to_name + "!\n");
	else
		builded.append("Kedves, Mrs " + to_name + "!\n");


	if (to_age < 12) {
		// TODO: Muszáj átkonvertálni a számot szöveggé a beillesztés miatt.
		stringstream sstm;
		sstm << to_age + 1;
		string next_age = sstm.str();
		builded.append("A következő évben már " + next_age + " éves leszel!");
	} else if (to_age == 17)
		builded.append("A következő évben már szavazhatsz!");
	else if (to_age > 70)
		builded.append("Remélem élvezed a nyugdíjas éveidet!");
	
	builded.append("\nMinden tisztelettel,\n\n" + name);
	
	// TODO: Kiírjuk a levél tartalmát.
	cout << builded;	
}